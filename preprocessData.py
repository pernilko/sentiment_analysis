import pandas as pd
import numpy as np
import cv2
from keras.preprocessing import image
import matplotlib.pyplot as plt
import os
from skimage.transform import rescale, resize
import shutil
import tensorflow as tf

emotion_label = ["neutral", "happiness", "anger", "sadness", "surprise"]

'''
Method for dividing data retrived from CSV-file into test and training data.
Returns two variables, one containing the test-data and one containing the training data.
'''
def splitDataFromCSV():
    df = pd.read_csv('labels.csv')
    df.drop(columns="user.id")
    df['emotion'] = df['emotion'].str.lower()

    validation_data = df.sample(frac=0.1)
    train_data = df[~df['image'].isin(validation_data['image'])]
    test_data = train_data.sample(frac=0.1)
    train_data = train_data[~train_data['image'].isin(test_data['image'])]

    return test_data, train_data, validation_data

'''
Method for generation of directories. The method makes a mainfolder for the data.
Then it creates two subfolders for training and test data. In each of these subfolders,
it also creates a subfolder for the 5 emotions. 
'''
def make_dir():
    os.mkdir('data')
    os.mkdir('data/train')
    os.mkdir('data/test')
    os.mkdir('data/validation')
    for emotion in emotion_label:
        train_cat = 'data/train/' + emotion
        test_cat = 'data/test/' + emotion
        val_cat = 'data/validation/' + emotion
        os.mkdir(train_cat)
        os.mkdir(test_cat)
        os.mkdir(val_cat)

'''
Method for copying images from the images folder into its respective subfolders in the data folder.
In this method some emotions are excluded to reduce the dataset's overall size. 
'''
def insert_images():
    test_data, train_data, val_data = splitDataFromCSV()

    counter_neutral = 0
    stop_neutral = False
    for index, row in train_data.iterrows():
        image = row['image']
        emotion = row['emotion']

        dest_img_path = 'data/train/' + emotion + '/' + image
        src_img_path = 'images/' + image

        if os.path.exists(src_img_path) and (emotion != "contempt" and emotion != "disgust" and emotion != "fear"):
            if emotion == "neutral":
                counter_neutral += 1
                if counter_neutral <= 3600:
                    shutil.copy(src_img_path, dest_img_path)
            else:        
                shutil.copy(src_img_path, dest_img_path)
        

    for index, row in test_data.iterrows():
        image = row['image']
        emotion = row['emotion']

        dest_img_path = 'data/test/' + emotion + '/' + image
        src_img_path = 'images/' + image

        if os.path.exists(src_img_path) and (emotion != "contempt" and emotion != "disgust" and emotion != "fear"):
            shutil.copy(src_img_path, dest_img_path)

    for index, row in val_data.iterrows():
        image = row['image']
        emotion = row['emotion']

        dest_img_path = 'data/validation/' + emotion + '/' + image
        src_img_path = 'images/' + image

        if os.path.exists(src_img_path) and (emotion != "contempt" and emotion != "disgust" and emotion != "fear"):
            shutil.copy(src_img_path, dest_img_path)


'''
Method for retriving training data as a datagenerator object. 
The method normalizes the data, and returns the generator. 
'''
def preprocess_train():
    dataGenerator = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
    )

    train_gen = dataGenerator.flow_from_directory(
        'data/train',
        target_size=(32,115),
        batch_size=64,
        class_mode="categorical",
        shuffle=True
    )


    return train_gen

'''
Method for retriving validation data as a datagenerator object. 
The method normalizes the data, and returns the generator. 
'''
def preprocess_validation():
    dataGenerator = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1. / 255,
    )

    val_gen = dataGenerator.flow_from_directory(
        'data/validation',
        target_size=(32, 115),
        batch_size=64,
        class_mode="categorical",
        shuffle=True
    )

    return val_gen

'''
Method for retriving test data as a datagenerator object. 
The method normalizes the data, and returns the generator. 
'''
def preprocess_test():
    dataGenerator = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
    )
    print("hei")
    return dataGenerator.flow_from_directory(
        'data/lessTest',
        target_size=(32,115),
        batch_size=55,
        class_mode="categorical",
        shuffle=True
    )


if __name__ == "__main__":
    make_dir()
    insert_images()