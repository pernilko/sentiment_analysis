import pandas as pd
import numpy as np
import cv2
from keras.preprocessing import image
import matplotlib.pyplot as plt
import os
from skimage.transform import rescale, resize
from tqdm import tqdm


face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
eyePair_cascade = cv2.CascadeClassifier('haarcascade_mcs_eyepair_big.xml')

'''
Method for retriving only eyes from an image using Haar Cascade algorithms. 
It takes the name of the input file and the name the ouput file as parameters.
The method writes the new picture to the output file specified.
'''

def return_eye_pair(infile, outfile):
    img = cv2.imread(infile)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
     
    faces = face_cascade.detectMultiScale(gray,scaleFactor=1.01, minNeighbors=3)

    if len(faces) == 0: 
        #print('didnt find face')
        return
    for x,y,w,h in faces:
    	roi_gray = gray[y:y+h, x:x+w]
    	roi_color = img[y:y+h, x:x+w]
    	eyes = eyePair_cascade.detectMultiScale(roi_gray)
    	if len(eyes) == 0:
            #print("didnt find eyes")
            return	
    	for (ex,ey,ew,eh) in eyes:
    		eyes_roi = roi_color[ey: ey+eh, ex:ex + ew]
    resizeImg = resize(eyes_roi, (50, 180, 3), preserve_range=True)
    cv2.imwrite(outfile, resizeImg)
	
	
'''
Method for loading images from the original images folder. 
It runs the images found through the return_eye_pair() method. 
'''    
def load_images_from_folder(folder):
    outFolder = 'images'
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            return_eye_pair(os.path.join(folder,filename), os.path.join(outFolder,filename))
            


def main():
    folder = 'images_org'
    load_images_from_folder(folder)

 
if __name__ == '__main__':
    main()