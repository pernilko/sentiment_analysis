from numpy import expand_dims
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import ImageDataGenerator
from matplotlib import pyplot
import cv2
import os
import csv

'''
Project: https://machinelearningmastery.com/how-to-configure-image-data-augmentation-when-training-deep-learning-neural-networks/
Code Author: Jason Brownlee
'''

'''
Method for data augmentation that vertically shifts the image
'''
def verticalAugment(img, outFolder, numberOfDup, filename):
    #outFolder = 'data/augment_test'
    # convert to numpy array
    filename = filename[:-4]
    data = img_to_array(img)
    # expand dimension to one sample
    samples = expand_dims(data, 0)
    # create image data augmentation generator
    datagen = ImageDataGenerator(height_shift_range=0.1)
    # prepare iterator
    it = datagen.flow(samples, batch_size=1)
    # generate samples and plot
    for i in range(numberOfDup):
        # generate batch of images
        batch = it.next()
        # convert to unsigned integers for viewing
        image = batch[0].astype('uint8')

        converted_num = f'{i}'
        emotion = os.path.basename(outFolder)
        cv2.imwrite(outFolder + "/" + filename + '_vertical_' + converted_num + '.jpg', image)

        with open('labels.csv', 'a+') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(['pernilko', filename + '_vertical_' + converted_num + '.jpg', emotion])

'''
Method for data augmentation that horisontally shifts the image
'''
def horisontalAugment(img, outFolder, numberOfDup, filename):
    filename = filename[:-4]
    # convert horisontal
    data = img_to_array(img)
    # expand dimension to one sample
    samples = expand_dims(data, 0)
    # create image data augmentation generator
    datagen = ImageDataGenerator(height_shift_range=0.1)
    # prepare iterator
    it = datagen.flow(samples, batch_size=1)
    # generate samples and plot
    for i in range(numberOfDup):
        # generate batch of images
        batch = it.next()
        # convert to unsigned integers for viewing
        image = batch[0].astype('uint8')
        converted_num = f'{i}'
        emotion = os.path.basename(outFolder)
        cv2.imwrite(outFolder + "/" + filename + '_horisontal_' +  converted_num + '.jpg', image)

        with open('labels.csv', 'a+') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

            spamwriter.writerow(['pernilko', filename + '_horisontal_' + converted_num + '.jpg', emotion])

        
'''
Method for data augmentation that varies the lighting
'''
def lightingAugment(img, outFolder, numberOfDup, filename):
    filename = filename[:-4]
    data = img_to_array(img)
    # expand dimension to one sample
    samples = expand_dims(data, 0)
    # create image data augmentation generator
    datagen = ImageDataGenerator(brightness_range=[0.40, 0.60])
    # prepare iterator
    it = datagen.flow(samples, batch_size=1)
    # generate samples and plot
    for i in range(numberOfDup):
        # generate batch of images
        batch = it.next()
        # convert to unsigned integers for viewing
        image = batch[0].astype('uint8')
        converted_num = f'{i}'
        emotion = os.path.basename(outFolder)
        cv2.imwrite(outFolder + "/" + filename + '_lighting_' + converted_num + '.jpg', image)

        with open('labels.csv', 'a+') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

            spamwriter.writerow(['pernilko', filename + '_lighting_' + converted_num + '.jpg', emotion])

   
'''
Method for data augmentation that flips the image
'''
def flipAugment(img, outFolder, numberOfDup, filename):
    filename = filename[:-4]
    # convert to numpy array
    data = img_to_array(img)
    # expand dimension to one sample
    samples = expand_dims(data, 0)
    # create image data augmentation generator
    datagen = ImageDataGenerator(horizontal_flip=True)
    # prepare iterator
    it = datagen.flow(samples, batch_size=1)
    # generate samples and plot
    for i in range(numberOfDup):
        # generate batch of images
        batch = it.next()
        # convert to unsigned integers for viewing
        image = batch[0].astype('uint8')
        converted_num = f'{i}'
        emotion = os.path.basename(outFolder)
        cv2.imwrite(outFolder + "/" + filename + '_flip_' + converted_num + '.jpg', image)

        with open('labels.csv', 'a+') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(['pernilko', filename + '_flip_' +  converted_num + '.jpg', emotion])


if __name__ == "__main__":
   
    # For anger
    # 73*6*4*2=3504
    folder = 'data/train/anger'
    for filename in os.listdir(folder):
       print("filename: ", filename)
       img = cv2.imread(os.path.join(folder,filename))
       flipAugment(img,folder,5, filename)
    for filename in os.listdir(folder):
       img = cv2.imread(os.path.join(folder,filename))
       verticalAugment(img,folder,3, filename)
    for filename in os.listdir(folder):
       img = cv2.imread(os.path.join(folder,filename))
       horisontalAugment(img,folder,1, filename)

    
    #For sadness
    #64*3*3*6=3456
    folder5 = 'data/train/sadness'
    for filename in os.listdir(folder5):
        img = cv2.imread(os.path.join(folder5,filename))
        lightingAugment(img,folder5,5, filename)
    for filename in os.listdir(folder5):
       img = cv2.imread(os.path.join(folder5,filename))
       verticalAugment(img,folder5,2, filename)
    for filename in os.listdir(folder5):
       img = cv2.imread(os.path.join(folder5,filename))
       flipAugment(img,folder5,2, filename)
    
    #For surprise
    #146*3*2*2*2=3504
    folder6 = 'data/train/surprise'
    for filename in os.listdir(folder6):
        img = cv2.imread(os.path.join(folder6,filename))
        lightingAugment(img,folder6,2, filename)
    for filename in os.listdir(folder6):
       img = cv2.imread(os.path.join(folder6,filename))
       horisontalAugment(img,folder6,1, filename)
    for filename in os.listdir(folder6):
        img = cv2.imread(os.path.join(folder6,filename))
        flipAugment(img,folder6,1, filename)
    for filename in os.listdir(folder6):
       img = cv2.imread(os.path.join(folder6,filename))
       verticalAugment(img,folder6,1, filename)


    # For neutral
    folder8 = 'data/train/neutral'
    index = 1
    for filename in os.listdir(folder8):
        if index > 1210:
            break
        img = cv2.imread(os.path.join(folder8, filename))
        lightingAugment(img, folder8, 1, filename)
        index += 1

    #For happiness
    #3605+950=4555
    folder7 = 'data/train/happiness'
    index = 1
    for filename in os.listdir(folder7):
        if index > 912:
            break
        img = cv2.imread(os.path.join(folder7,filename))
        lightingAugment(img,folder7,1, filename)
        index += 1

