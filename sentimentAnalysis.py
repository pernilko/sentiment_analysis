from keras.applications.vgg16 import VGG16, decode_predictions, preprocess_input
from keras.models import Sequential, load_model
from keras.layers import Flatten, Dense, Dropout, Conv1D, Conv2D
from keras import optimizers
import keras
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from preprocessData import preprocess_train, preprocess_test, preprocess_validation
from tensorflow.keras.layers.experimental.preprocessing import Normalization
from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd
from tensorflow.keras import regularizers

emotion_label = ["Anger", "Happiness", "Neutral", "Sadness", "Surprise"]

#facenet_model = load_model('facenet_keras.h5')

'''
Retrieving train, test and validation generator
'''
train = preprocess_train()
test_gen = preprocess_test()
validation = preprocess_validation()
#print("x_train=%i, y_train=%i" % (len(x_train), len(y_train)))

'''
Iterating over generators
'''
image_batch, label_batch = next(iter(train))
#print(image_batch)
image_val_batch, label_val_batch = next(iter(validation))
testX, testY = next(iter(test_gen))

'''
Class for convolution neural network using the pretrained model VGG16 
'''
class CNN(tf.keras.Model):
    def __init__(self):
        super(CNN, self).__init__()
       
        self.VGG = VGG16(weights='imagenet', include_top=False, input_shape=(32, 115, 3))
        self.model = Sequential()
        counter = 0
        for layer in self.VGG.layers[:-4]:
            if counter < 13:
                layer.trainable = False
            else:
                layer.trainable = True
            #layer.trainable = False
            self.model.add(layer)
            counter += 1
        for layer in self.model.layers:
            print(layer.name, ", ", layer.trainable)
        
        # Model layers
        self.model.add(tf.keras.layers.MaxPooling2D(pool_size=2))
        self.model.add(tf.keras.layers.Dropout(0.5))
        self.model.add(tf.keras.layers.Flatten())
       
        self.model.add(tf.keras.layers.Dense(256, activation=tf.nn.relu))
        self.model.add(tf.keras.layers.Dropout(0.5))
        self.model.add(tf.keras.layers.Dense(5, activation='softmax'))
        
    '''
    Performs the logic of applying the layer to the input tensors
    '''
    def call(self, x):
        x = self.model(x)
        self.model.summary()
      
        return x


model = CNN()
'''
Adam is a replacement optimization algorithm for stochastic gradient descent for training deep learning models.
They tie together the loss function and model parameters by updating the model in response to the output of the loss function.
'''
optimizerAdam = tf.keras.optimizers.Adam(learning_rate=0.001, clipnorm=1.0)

model.compile(loss='categorical_crossentropy',
              optimizer=optimizerAdam,
              metrics=['acc'])  # Compiling the model


num_of_epochs = 20
batch_size = 64

history = model.fit(train, validation_data = validation, batch_size=batch_size, epochs=num_of_epochs, shuffle=True)  # Training the model
res = model.evaluate(test_gen, batch_size=batch_size)  # Evaluating the test accuracy of the model
print("Test loss, test acc: ", res)

model.save("Models/model.h5py")  # Saves the model architecture, training configuration and model weights

prediction = model.predict(testX)  # Generates output predictions for the input samples
print("Predictions Tabel: ", prediction)


# Converting numerical encodings to categorical labels for the predictions
pred = []
for x in prediction:
    x_max = x.argmax()
    print("prediction = %s" % emotion_label[x_max])
    pred.append(emotion_label[x_max])


# Converting numerical encodings to categorical labels for the actual values
cor = []
for x in testY:
    print("actual = %s " % emotion_label[x.argmax()])
    cor.append(emotion_label[x.argmax()])



train_acc = history.history['acc']
test_acc = history.history['val_acc']
train_loss = history.history['loss']
test_loss = history.history['val_loss']

# Plot model accuracy
plt.plot(train_acc)
plt.plot(test_acc)
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'])
plt.savefig('accuracy_attempt_9.png')
plt.show()

# Plot model loss
plt.plot(train_loss)
plt.plot(test_loss)
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'])
plt.savefig('loss_attempt_9.png')
plt.show()

# Plotting the confusion matrix
c_matrix = confusion_matrix(cor, pred, labels=emotion_label)
print(c_matrix)
df = pd.DataFrame(c_matrix, index = emotion_label, columns = emotion_label)
sn.heatmap(df, annot=True, cmap = "Blues", robust=True)
plt.savefig('confusionmatrix_attempt_9.png')
plt.show()

# Plotting the test images with the model predictions
plt.figure(figsize=(10,9))
plt.subplots_adjust(hspace=0.5)
for n in range(len(testX)):
    plt.subplot(8,7,n+1)
    plt.imshow(testX[n])
    plt.title(pred[n].title())
    plt.axis('off')
    _ = plt.suptitle("Model predictions")
plt.savefig('predictions_attempt_9.png')

# Plotting the test images with actual labels
plt.figure(figsize=(10,9))
plt.subplots_adjust(hspace=0.5)
for n in range(len(testX)):
    plt.subplot(8,7,n+1)
    plt.imshow(testX[n])
    max = np.argmax(testY[n])
    plt.title(emotion_label[max].title())
    plt.axis('off')
    _ = plt.suptitle("Model actual")
plt.savefig('actual_attempt_9.png')

